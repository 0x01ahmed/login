const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const User = require('./models/user');
require('dotenv').config()
const app = express();

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.render('index',{error: ''});
});

app.post('/', async (req, res) => {
  const { username, password } = req.body;
  const user = "admin";
  const pass = "$up3r_S3cret_p4$$w0rd";
  if (username === user && password === pass) {
    res.redirect('/dashboard');
  } else {
    res.render('index', { error: 'Invalid Credentials' });
  }
});
app.get('/dashboard', (req,res,next) => {

 res.render('dashboard',{isAuthenticated:true,flag: `CTF{${process.env.FLAG}}`,error: null});
});

app.listen(3000, () => {
  console.log('Server is running on http://localhost:3000');
});
